# Bool Tool Add-on

This add-on was part of [Blender 4.1 bundled add-ons](https://docs.blender.org/manual/en/4.1/addons/). This is now available as an extension on the [Extensions platform](https://extensions.blender.org/add-ons/bool-tool).

This is an archived repository for version of add-on that was shipped with Blender until Blender 4.2.

Add-on is now developed on [Github](https://github.com/nickberckley/bool_tool) and listed on extensions platform.
